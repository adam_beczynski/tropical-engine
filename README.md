WARNING! Currently project is stored at Perforce server at ec2-35-164-134-176.us-west-2.compute.amazonaws.com:1666 which can be accessed using Guest account.

# Tropical Engine - OpenGL4 C++ 3D Game Engine #

Author: Adam "Fartuess" Beczyński

"TropicalEngineComponentArchitecture" - Current Project

"Tropical Apartment Scene" - Old OpenGL3 Project.

### Languages technologies and libraries used ###
* C++
* OpenGL4
* GLSL
* Qt5
* Open Asset Importer
* GLEW
* GLM

### Features ###
#### Architecture and Gameplay Scripting ####
* Component architecture
* Easy gameplay coding with IUpdate'able interface
* Input Management System which allows creating abstract input actions and binding physical keys to them.
* Level and asset serialization
#### Shaders and Materials ####
* Flexible system of managing shaders and materials
* Shaders builders allow creating shaders from modules. Shader builder can create shaders for multiple passes from one set of modules.
* Various lighting models (Phong, Blinn-Phong, Cook-Torrance, Strauss, Isotropic Ward, Anisotropic Ward)
* Bump mapping (normal mapping)
* Steep Parralax Mapping
* Alpha testing (aka masked translucency)
* Adaptive tessalation
* Vertex displacement and vector displacement.
* Directional lights
* Point lights
#### Interface ####
* Qt interface
* Custom style of interface
* Custom window frame
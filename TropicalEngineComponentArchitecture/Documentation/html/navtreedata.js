var NAVTREE =
[
  [ "Tropical Engine", "index.html", [
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"class_tropical_engine_1_1_input_manager.html#a799e49d0a0f8ee03c7ce1f92ef408d2e",
"class_tropical_engine_1_1_scene.html#a7fd8c9ead7bf5e0af0c8e64fa995e231",
"class_tropical_engine_1_1_tropical_engine_application.html#a7e5fbcae31839fd1a3d1e4293b1369bc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';